﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_Application_4.Models
{
    public class Tag
    {
        public int ID { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Comment có từ 10 đến 100 ký tự !!", MinimumLength = 10)]
        public String Content { get; set; }

        //
        public virtual ICollection<Post> Posts { get; set; }
    }
}