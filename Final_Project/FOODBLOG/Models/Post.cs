﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FOODBLOG.Models
{
    public class Post
    {
        public int ID { get; set; }
        public String Title { get; set; }
        public String Body { get; set; }
        public DateTime DateCreated { get; set; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfilerUserID { get; set; }
        
    }
}