﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FOODBLOG.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "WELCOME TO FOOD&REVIEW.COM";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Trang About";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Liên hệ";

            return View();
        }

        public ActionResult Review()
        {
            ViewBag.Message = "Trang review các quán ăn nhà hàng";
            return View();
        }
    }
}
