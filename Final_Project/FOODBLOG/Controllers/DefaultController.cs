﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FOODBLOG.Controllers
{
    public class DefaultController : Controller
    {
        public ActionResult Default()
        {
            ViewBag.a = "This is our homepage";
            return View();
        }

        public ActionResult TrangChu()
        {
            return View();
        }

        public ActionResult Review()
        {
            return View();
        }
        public ActionResult TinTuc()
        {
            return View();
        }
        public ActionResult Forum()
        {
            return View();
        }
        public ActionResult KhuyenMai()
        {
            return View();
        }
        public ActionResult GioiThieu()
        {
            return View();
        }
        public ActionResult QuanAn()
        {
            return View();
        }
        public ActionResult NhaHang()
        {
            return View();
        }
        public ActionResult VietNam()
        {
            return View();
        }
        public ActionResult TheGioi()
        {
            return View();
        }
        public ActionResult BV1()
        {
            return View();
        }
        public ActionResult BV2()
        {
            return View();
        }
        public ActionResult BV3()
        {
            return View();
        }
        public ActionResult BV4()
        {
            return View();
        }
        public ActionResult QuangCao()
        {
            return View();
        }
    }
}
