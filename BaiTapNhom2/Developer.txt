Trang web: review về các nhà hàng, quán ăn đồng thời cung cấp các thông tin về ẩm thực trong nước cũng như trên
thế giới. Đem tới cho người truy cập những thông tin khuyến mãi từ các nhà hàng, quán ăn, những địa chỉ ẩm 
thực đang được nhiều người quan tâm. 

Mục đích: 
*Đối với người dùng:
- Tạo ra một nơi uy tín để mọi người trước khi đi ăn ở đâu có thể vào để xem review về nơi mà mình chuẩn 
bị tới. Giúp mọi người tiết kiệm được thời gian, không cần phải tới đúng địa chỉ quán ăn mà vẫn có thể biết trước được 
chât lượng món ăn, thái độ phục vụ như thế nào... 
- Cung cấp thông tin về các gói khuyến mãi, giảm giá ở các quán ăn, nhà hàng giúp khách cập nhập được nhanh và 
chính xác nhất. 
- Cập nhật những tin tức về ẩm thực, các xu hướng ăn uống đang thịnh hành ở Việt Nam cũng như trên thế giới. 
- Cung cấp những mẹo nhỏ cho mọi người khi đi du lịch ở các nước khác hoặc du lịch trong các tỉnh trong nước. Các 
địa chỉ nên đến để thưởng thức những đặc sản địa phương ngon mà không bị "chém" hoặc không được phục vụ tốt.
- Giúp người dùng tương tác được với nhau thông qua các bài viết, mọi người có thể hỏi những câu hỏi mình muốn.
Có thể comment trên một bài review bất kì nếu có ý kiến gì đó. 
*Đối với nhà đầu tư: họ hoàn toàn có thể thu lại số vốn đầu tư ban đầu nhờ vào số tiền thu được từ quảng cáo, đăng 
tin khuyến mãi. Trang web được thiết kế với nhiều chỗ để đặt các banner quảng cáo. Vì vậy nếu PR cho trang web tốt
thì số tiền thu được từ quảng cáo sẽ không nhỏ. Đồng thời sẽ xây dựng thêm ứng dụng của trang web trên di động, từ đó 
thu thêm lợi nhuận quảng cáo trên di động.

Cách hoạt động: 
- Người dùng bình thường khi chưa đăng nhập có thể xem bài viết, đọc các tin tức, các thông tin khuyến mãi, các banner 
hình ảnh quảng cáo.
- Người dùng khi đăng nhập vào có thể đăng tải bài viết review lên forum của trang web. Có thể bình luận, bấm "Like" cho
các bài review mà mình cảm thấy thích hoặc bấm "Report" cho bài mình cảm thấy vi phạm điều khoản của forum.
- Các trang review nổi bật, nhiều lượt "Like", comment sẽ được xuất hiện trên trang chủ của web.
- Mỗi thành viên khi đăng bài, được nhấn "Like" sẽ được cộng điểm. Tuỳ vào số điểm mà thành viên sẽ được thăng cấp
bậc khác nhau như: 
+ Mũ đen: thành viên mới
+ Mũ trắng: thành viên hoạt động nhiều
+ Mũ xanh: thành viên hoạt động tích cực.
+ Mũ vàng: thành viên có nhiều đóng góp quan trọng.
+ Mũ đỏ: thành viên kì cựu
- Những thành viên đăng bài spam, bài quảng cáo trá hình, bài bị report nhiều sẽ bị hạ bậc. Nếu vi phạm liên tục sẽ bị 
ban nick vĩnh viễn.
- Khoảng 6 tháng sẽ tổ chức một buổi off line để giao lưu giữa các thành viên trong forum, tăng tính đoàn kết. Kết hợp 
khen thưởng cho các cá nhân có nhiều hoạt động.

Thu lợi nhuận: 
- Nguồn thu chính là từ quảng cáo. Các đơn vị, cá nhân muốn quảng cáo trên trang chủ của web sẽ trả tiền. Số tiền trả 
dựa vào độ lớn và vị trí của banner. Các banner ở vị trí trung tâm sẽ có giá cao hơn các banner ở bên góc.
- Các quán ăn, nhà hàng muốn đăng tin khuyến mãi, quảng cáo cũng liên hệ với web để được đăng tin lên. Tuỳ thuộc vào số tiền
mà tin khuyến mãi của nhà hàng đó được ghim trong mục khuyến mãi ở trang đầu trong nhiều ngày hoặc sẽ trôi dần xuống.