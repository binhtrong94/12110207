namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Password = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 250),
                        LastName = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.BaiViet", "AccountID", c => c.Int(nullable: false));
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false));
            AddForeignKey("dbo.BaiViet", "AccountID", "dbo.Accounts", "ID", cascadeDelete: true);
            CreateIndex("dbo.BaiViet", "AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.BaiViet", new[] { "AccountID" });
            DropForeignKey("dbo.BaiViet", "AccountID", "dbo.Accounts");
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.BaiViet", "AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
