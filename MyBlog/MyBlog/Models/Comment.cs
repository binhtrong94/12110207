﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyBlog.Models
{
    public class Comment
    {
        [Key]
        public int ID { set; get; }
        public int Post_id { set; get; }
        public String NoiDung { set; get; }
    }
}